import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <div className="content has-text-centered">
          <p>
            <strong>GGMM</strong> All Rights Reserved.
          </p>
        </div>
      </footer>
    );
  }
}
