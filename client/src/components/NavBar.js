import React, { Component } from "react";
import { Link } from "react-router-dom";
// import { app } from "../services/flamelink";
import logo from "../assets/logo.svg";

export default class NavBar extends Component {
  // constructor() {
  //   super();
  //   this.state = {
  //     logo: ""
  //   };
  //
  //   app.content.get("header").then(headerLogo =>
  //     app.storage
  //       .getURL(headerLogo.headerLogo[0])
  //       .then(url =>
  //         this.setState({
  //           logo: url
  //         })
  //       )
  //       .catch(error =>
  //         console.error(
  //           "Something went wrong while retrieving the file URL. Details:",
  //           error
  //         )
  //       )
  //   );
  // }

  render() {
    return (
      <header>
        <link
          rel="stylesheet"
          href="https://pro.fontawesome.com/releases/v5.1.0/css/all.css"
          integrity="sha384-87DrmpqHRiY8hPLIr7ByqhPIywuSsjuQAfMXAE0sMUpY3BM7nXjf+mLIUSvhDArs"
          crossOrigin="anonymous"
        />
        <div
          className="navbar is-fixed-top"
          role="navigation"
          aria-label="dropdown navigation"
        >
          <Link to="/" className="navbar-item">
            <img
              src={logo}
              alt="Bulma: a modern CSS framework based on Flexbox"
            />
          </Link>

          <div className="navbar-item has-dropdown is-hoverable">
            <a className="navbar-link">Dropdown</a>

            <div className="navbar-dropdown">
              <Link className="navbar-item" to="/checklist">
                Dropdown Item
              </Link>
            </div>
          </div>
          <Link className="navbar-item" to="/contact">
            Contact
          </Link>
        </div>
      </header>
    );
  }
}
