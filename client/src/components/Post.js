import React, { Component } from "react";
import { Link } from "react-router-dom";
import { app } from "../services/flamelink";

import RecentPosts from "./RecentPosts";
import SocialBar from "./SocialBar";
import _ from "lodash";
import Head from "./Head";

export default class Posts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      post: []
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    const { id } = this.props.match.params;

    app.content
      .getByField("blogPost", "slug", id, {
        populate: ["featuredImage"]
      })
      .then(post =>
        this.setState({
          post: Object.values(post)
        })
      );
  }

  renderFeaturedImage() {
    return _.map(this.state.post, post => {
      return _.map(post.featuredImage, image => {
        if (image.url) {
          return (
            <div
              className="creative"
              style={{
                backgroundImage: "url(" + image.url + ")",
                backgroundSize: "cover"
              }}
            />
          );
        }
      });
    });
  }

  convertDate(date) {
    const removeTime = date.substring(0, date.indexOf("T"));
    const p = removeTime.split(/\D/g);
    return [p[1], p[2], p[0]].join("-");
  }

  convertToSlug(Text) {
    return Text.toLowerCase()
      .replace(/ /g, "-")
      .replace(/&/g, "and")
      .replace(/[^\w-]+/g, "");
  }

  componentDidUpdate() {
    window.scrollTo(0, 0);
  }

  render() {
    if (!this.state.post) {
      return <div>Loading...</div>;
    }
    return _.map(this.state.post, post => {
      return _.map(post.featuredImage, image => {
        return (
          <div key={post.title}>
            <div className="bg-nav" />
            <Head
              title={post.title}
              ogImage={image.url}
              url={window.location.href}
              desc={post.summary}
              mTitle={post.title}
            />
            <div className="post-banner">
              <div className="meta-info">
                <Link
                  to={"/category/" + this.convertToSlug(post.category)}
                  className="reveal-text cat-link"
                >
                  {post.category}
                </Link>
                <div
                  className="block"
                  style={{ display: "block", margin: "0px" }}
                />
                <h1 className="reveal-text">{post.title}</h1>
                <div
                  className="block"
                  style={{ display: "block", margin: "0px" }}
                />
                <div className="reveal-text meta-data">
                  <h3>
                    <span>Author</span>
                    {post.author} | <span>Published</span>
                    {this.convertDate(post.date)}
                  </h3>
                </div>
                <h2 className="reveal-text">{post.summary}</h2>
              </div>
              {this.renderFeaturedImage()}
            </div>
            <div className="post_wrapper">
              <div className="content_wrapper">
                <div className="share-box">
                  <SocialBar
                    fbLink={window.location.href}
                    fbTitle={post.title}
                    display="block"
                    position="fixed"
                    summary={post.summary}
                    sharing={true}
                  />
                </div>
                <div id="post-id" className="content">
                  <div
                    className="inner_content"
                    dangerouslySetInnerHTML={{ __html: post.content }}
                  />
                </div>
              </div>
            </div>
            <RecentPosts init={3} />
          </div>
        );
      });
    });
  }
}
