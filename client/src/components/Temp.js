import React, { Component } from "react";
import "../temp.css";

export default class Temp extends Component {
  render() {
    return (
      <div className="hero is-fullheight">
        <div className="container" style={{ marginTop: "100px" }}>
          <ul
            style={{
              padding: "40px",
              backgroundColor: "white",
              borderRadius: "10px"
            }}
          >
            <h1>Tips and Tricks</h1>
            <li>
              Replace /services/flamelink and firebase with your new API
              credentials
            </li>
            <li>
              Use the boilerplate express code here along with prerender.io to
              support better SEO
            </li>
            <li>Delete this component and temp.css</li>
            <li>
              Use Flamelink's app.get within componentDidMount unless the app
              calls for more complex state management
            </li>
            <li>Plan out components prior to development</li>
            <li>Don't hardcode data in components. Pass them through props</li>
            <li>Remember to remove console.logs</li>
            <li>Delete unused packages and imports to avoid bloat</li>
            <li>Don't join a cult unless they seem legit</li>
            <li>Test your SEO with Facebook debugger</li>
            <li>
              Make sure your pages have Head information (see the Head.js
              component)
            </li>
            <li>Pat dogs</li>
            <li>
              Having a plan for your data structure will save you headaches and
              time
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
