const express = require("express");
const sgMail = require("@sendgrid/mail");
const Mailchimp = require("mailchimp-api-v3");
require("dotenv").config();
const bodyParser = require("body-parser");
const path = require("path");
const port = process.env.PORT || 3001;

var mc_api_key = process.env.MAILCHIMP_API_KEY;
var list_id = process.env.MAILING_LIST_ID;
var sendgrid_api_key = process.env.SENDGRID_API_KEY;

const email_template = "./email.html";

const app = express();
const mailchimp = new Mailchimp(mc_api_key);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//prerender

app.use(
  require("prerender-node").set("prerenderToken", "5p6S6OHBH3Onx3e1zctD")
);

//sending email

app.post("/sendmail", (req, res) => {
  sgMail.setApiKey(sendgrid_api_key);
  const msg = {
    to: "tyler@ggmm.io", //receiver's email
    from: req.body.email_address, //sender's email
    subject: "New Website Inquiry from" + req.body.fname + req.body.lname, //Subject
    html: email_template
  };
  sgMail.send(msg);
});

//fixing routes server-side

app.use(express.static(path.join(__dirname, "client/build")));

app.get("/*", function(req, res) {
  res.sendFile(path.join(__dirname, "client/build", "index.html"));
});

//mailchimp api

app.post("/api/mc", (req, res) => {
  mailchimp
    .post(`/lists/${list_id}/members`, {
      email_address: req.body.email,
      status: "subscribed",
      merge_fields: {
        FNAME: req.body.fname,
        LNAME: req.body.lname
      }
    })
    .then(function(results) {
      res.send(results);
    })
    .catch(function(err) {
      res.send(err);
    });
});

app.listen(port, () => console.log(`Listening on port ${port}`));
